//
//  ViewController.m
//  Side_Menu
//
//  Created by Clicklabs 104 on 11/20/15.
//  Copyright (c) 2015 cl. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize secondview;
@synthesize button;

- (void)viewDidLoad {
    [super viewDidLoad];
    secondview.hidden=YES;
    // Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)button:(id)sender {
    
    secondview.hidden=NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
