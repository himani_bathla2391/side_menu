//
//  viewFile.m
//  Side_Menu
//
//  Created by Clicklabs 104 on 11/20/15.
//  Copyright (c) 2015 cl. All rights reserved.
//

#import "viewFile.h"
UITableView *table;
NSArray *data;
UITableViewCell * cell;
@implementation viewFile

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void) awakeFromNib{
    data= [[NSArray alloc]initWithObjects:@"Red",@"Yelllow",@"Black", nil];
    table=[[UITableView alloc]initWithFrame:CGRectMake(20, 30, 40, 100)];
    [table registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    table.delegate= self;
    table.dataSource= self;
    
    table.separatorStyle= UITableViewCellSeparatorStyleSingleLine;
    [table reloadData];
    [self addSubview:table];
}
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return data.count;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    cell = [tableView dequeueReusableCellWithIdentifier:@"cellReuse"];
    cell.textLabel.text = data[indexPath.row];
    return cell;
    
}
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //deselect row
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.row)
    {
        case 0:
            NSLog(@"red");
            break;
        case 1:
             NSLog(@"yellow");
            break;
        case 2:
             NSLog(@"black");
            break;
            
        default:
            break;
    }
}


@end
